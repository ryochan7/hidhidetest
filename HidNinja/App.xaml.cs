﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace HidNinja
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Check if root whitelist registry key exists
            Util.WriteWhitelistRoot();
            // Check for stale whitelist entries
            Util.PurgeWhitelistEntries(true);
            
            Util.ExportIdToRegistry(Util.MainProcessId);

            RenderOptions.ProcessRenderMode = RenderMode.SoftwareOnly;
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            // Delete self from whitelist entries
            Util.DeleteIdFromRegistry(Util.MainProcessId);
        }
    }
}
