﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HidNinja.ViewModels;

namespace HidNinja
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CurrentDevicesViewModel currentDevVM;
        private AffectedDevicesViewModel affectDevVM;

        public MainWindow()
        {
            InitializeComponent();

            currentDevVM = new CurrentDevicesViewModel();
            currentDevsListBox.DataContext = currentDevVM;
            currentDeviceInfo.DataContext = currentDevVM.CurrentDevice;
            currentDeviceInfo.Visibility = Visibility.Hidden;
            devActionPanel.DataContext = currentDevVM;
            //hardwareIdsListbox.DataContext = currentDevVM.CurrentDevice;

            affectDevVM = new AffectedDevicesViewModel();
            affectedListBox.DataContext = affectDevVM;

            affectedActionPanel.DataContext = affectDevVM;

            SetupEvents();
        }

        private void SetupEvents()
        {
            currentDevVM.CurrentDeviceChanged += CurrentDevVM_CurrentDeviceChanged;
            affectDevVM.AffectedHwidsCol.CollectionChanged += AffectedHwidsCol_CollectionChanged;
        }

        private void AffectedHwidsCol_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            currentDevVM.CurrentDevice?.RefreshAffectedState(affectDevVM.AffectedHwidsCol);
        }

        private void CurrentDevVM_CurrentDeviceChanged(object sender, EventArgs e)
        {
            currentDevVM.CurrentDevice.RefreshAffectedState(affectDevVM.AffectedHwidsCol);
            currentDeviceInfo.DataContext = currentDevVM.CurrentDevice;
            currentDeviceInfo.Visibility =
                currentDevVM.CurrentDevice != null ? Visibility.Visible : Visibility.Hidden;
            //hardwareIdsListbox.DataContext = currentDevVM.CurrentDevice;
        }

        private void AddHWIDBtn_Click(object sender, RoutedEventArgs e)
        {
            int idx = -1;
            if (currentDevVM.CurrentDevice != null)
            {
                idx = currentDevVM.CurrentDevice.SelectedIndex;
            }
            
            if (idx >= 0)
            {
                string hwid = currentDevVM.CurrentDevice.HardwareIds[idx];
                affectDevVM.CheckAddHWID(hwid);
            }
        }

        private void RemoveAffectHWID_Click(object sender, RoutedEventArgs e)
        {
            int idx = affectDevVM.SelectedIndex;
            if (idx >= 0)
            {
                affectDevVM.RemoveHWIDAt(idx);
            }
        }

        private void CommentAffectHWID_Click(object sender, RoutedEventArgs e)
        {
            int idx = affectDevVM.SelectedIndex;
            if (idx >= 0)
            {
                if (affectDevVM.CommentHWIDAt(idx))
                {
                    affectedListBox.DataContext = null;
                    affectedListBox.DataContext = affectDevVM;
                }
            }
        }

        private void UncommentAffectHWID_Click(object sender, RoutedEventArgs e)
        {
            int idx = affectDevVM.SelectedIndex;
            if (idx >= 0)
            {
                if (affectDevVM.UnCommentHWIDAt(idx))
                {
                    affectedListBox.DataContext = null;
                    affectedListBox.DataContext = affectDevVM;
                }
            }
        }

        private void ManualEditAffectHWID_Click(object sender, RoutedEventArgs e)
        {
            int idx = affectDevVM.SelectedIndex;
            if (idx >= 0)
            {
                string[] tmpArray = new string[1] { affectDevVM.AffectedHwidsCol[idx] };
                ManualEditWindow mew = new ManualEditWindow(tmpArray, false);
                mew.Saved += (s, newhwids) =>
                {
                    affectDevVM.AffectedHwidsCol[idx] = newhwids;
                    affectDevVM.ExportAffectedDevices();
                    affectedListBox.DataContext = null;
                    affectedListBox.DataContext = affectDevVM;
                };

                mew.ShowDialog();
            }
        }

        private void ManualBulkEditAffectHWID_Click(object sender, RoutedEventArgs e)
        {
            string[] tmpArray = affectDevVM.AffectedHwidsCol.ToArray();
            ManualEditWindow mew = new ManualEditWindow(tmpArray, true);
            mew.Saved += (s, newhwids) =>
            {
                affectedListBox.DataContext = null;
                affectDevVM.AffectedHwidsCol.Clear();
                foreach (string tmp in newhwids.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    affectDevVM.AffectedHwidsCol.Add(tmp);
                }

                affectDevVM.ExportAffectedDevices();
                affectedListBox.DataContext = affectDevVM;
            };

            mew.ShowDialog();
        }

        private void ClearAffectHWIDListBtn_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBoxResult.No;
            result = MessageBox.Show("Do you want to clear the AffectedDevices list?",
                "Confirm Clear AffectedDevices", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.Yes)
            {
                affectedListBox.DataContext = null;
                affectDevVM.AffectedHwidsCol.Clear();
                affectDevVM.ExportAffectedDevices();
                affectedListBox.DataContext = affectDevVM;
            }
        }
    }
}
