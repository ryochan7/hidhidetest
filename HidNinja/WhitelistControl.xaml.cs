﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HidNinja.ViewModels;

namespace HidNinja
{
    /// <summary>
    /// Interaction logic for WhitelistControl.xaml
    /// </summary>
    public partial class WhitelistControl : UserControl
    {
        private WhitelistViewModel whitelistVM;
        private bool escapeGrab = false;
        private bool runProcGrab = false;

        public WhitelistControl()
        {
            InitializeComponent();

            whitelistVM = new WhitelistViewModel();
            SetupListsDataContext();
        }

        private void NullListsDataContext()
        {
            currentProcessesListBox.DataContext = null;
            currentWhitelistListBox.DataContext = null;
        }

        private void SetupListsDataContext()
        {
            currentProcessesListBox.DataContext = whitelistVM;
            currentWhitelistListBox.DataContext = whitelistVM;
        }

        private void AllowBtn_Click(object sender, RoutedEventArgs e)
        {
            whitelistVM.AttemptAllow();
        }

        private void DenyBtn_Click(object sender, RoutedEventArgs e)
        {
            whitelistVM.AttemptDeny();
        }

        private void DenyAllBtn_Click(object sender, RoutedEventArgs e)
        {
            currentWhitelistListBox.DataContext = null;
            whitelistVM.AttemptDenyAll();
            currentWhitelistListBox.DataContext = whitelistVM;
        }

        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            NullListsDataContext();
            
            whitelistVM.RefreshLists();

            SetupListsDataContext();
        }

        private void GrabAppBtn_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Cross;
            runProcGrab = true;
            ObtainSwitchedProcessWindow();
        }

        private async void ObtainSwitchedProcessWindow()
        {
            uint procId = (uint)Process.GetCurrentProcess().Id;
            uint lpdwProcessId = procId;

            await Task.Run(() =>
            {
                IntPtr nuhandle = NativeMethodsNinja.GetForegroundWindow();
                while (procId == lpdwProcessId && !escapeGrab)
                {
                    Thread.Sleep(20);
                    nuhandle = NativeMethodsNinja.GetForegroundWindow();
                    NativeMethodsNinja.GetWindowThreadProcessId(nuhandle, out lpdwProcessId);
                }
            });

            if (!escapeGrab)
            {
                NullListsDataContext();
                whitelistVM.RefreshLists();
                whitelistVM.AttemptProcessAdd((int)lpdwProcessId);
                SetupListsDataContext();
            }

            runProcGrab = false;
            escapeGrab = false;
            Mouse.OverrideCursor = null;
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (runProcGrab && e.Key == Key.Escape)
            {
                escapeGrab = true;
                e.Handled = true;
            }
        }
    }
}
