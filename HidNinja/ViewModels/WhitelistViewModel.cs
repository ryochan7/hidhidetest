﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using HidNinja;

namespace HidNinja.ViewModels
{
    class WhitelistViewModel
    {
        private int blockedSelectedIndex = -1;
        public int BlockedSelectedIndex {
            get => blockedSelectedIndex;
            set => blockedSelectedIndex = value;
        }

        private ObservableCollection<ProcessInfo> blockedProcessList =
            new ObservableCollection<ProcessInfo>();

        private ObservableCollection<ProcessInfo> whitelistIds =
            new ObservableCollection<ProcessInfo>();

        public ObservableCollection<ProcessInfo> WhitelistIds { get => whitelistIds; }
        public ObservableCollection<ProcessInfo> BlockedProcessList { get => blockedProcessList; }
        private HashSet<int> whitelistSet = new HashSet<int>();
        private int whitelistSelectedIndex = -1;
        public int WhitelistSelectedIndex {
            get => whitelistSelectedIndex;
            set => whitelistSelectedIndex = value;
        }

        private int mainProcessId;

        public WhitelistViewModel()
        {
            mainProcessId = Process.GetCurrentProcess().Id;

            PopulateWhiteList();
            PopulateBlockedList();
        }

        private void PopulateWhiteList()
        {
            using (RegistryKey key =
                Registry.LocalMachine.OpenSubKey($@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters\Whitelist"))
            {
                if (key != null)
                {
                    string[] tmpids = key.GetSubKeyNames();
                    foreach (string id in tmpids.OrderBy(p => p))
                    {
                        if (int.TryParse(id, out int procId))
                        {
                            if (procId != mainProcessId)
                            {
                                try
                                {
                                    using (Process tempProc = Process.GetProcessById(procId))
                                    {
                                        whitelistIds.Add(new ProcessInfo(tempProc));
                                        whitelistSet.Add(procId);
                                    }
                                }
                                catch (ArgumentException) { }
                            }
                        }
                    }
                }
            }
        }

        private void PopulateBlockedList()
        {
            // Get all processes running on the local computer.
            Process[] localAll = Process.GetProcesses();
            foreach (Process tmpProc in localAll.ToList().OrderBy(p => p.ProcessName).ThenBy(p => p.Id))
            {
                if (!whitelistSet.Contains(tmpProc.Id) && tmpProc.Id != mainProcessId)
                {
                    blockedProcessList.Add(new ProcessInfo(tmpProc));
                }
            }
        }

        public void AttemptAllow()
        {
            if (blockedSelectedIndex >= 0)
            {
                int procId = blockedProcessList[blockedSelectedIndex].PID;
                whitelistIds.Add(blockedProcessList[blockedSelectedIndex]);
                whitelistSet.Add(procId);

                blockedProcessList.RemoveAt(blockedSelectedIndex);
                ExportIdToRegistry(procId);
            }
        }

        public void AttemptDeny()
        {
            if (whitelistSelectedIndex >= 0)
            {
                int procId = whitelistIds[whitelistSelectedIndex].PID;
                try
                {
                    using (Process tmpProc = Process.GetProcessById(procId))
                    {
                        blockedProcessList.Add(new ProcessInfo(tmpProc));
                    }
                }
                catch (ArgumentException) { }

                whitelistIds.RemoveAt(whitelistSelectedIndex);
                whitelistSet.Remove(procId);
                DeleteIdFromRegistry(procId);
            }
        }

        public void AttemptDenyAll()
        {
            foreach (ProcessInfo procInfo in whitelistIds)
            {
                int procId = procInfo.PID;
                try
                {
                    using (Process tmpProc = Process.GetProcessById(procId))
                    {
                        blockedProcessList.Add(new ProcessInfo(tmpProc));
                    }
                }
                catch (ArgumentException) { }
            }

            whitelistIds.Clear();
            whitelistSet.Clear();
            PurgeWhitelistEntries();
        }

        public void RefreshLists()
        {
            blockedProcessList.Clear();
            whitelistIds.Clear();
            whitelistSet.Clear();

            PopulateWhiteList();
            PopulateBlockedList();
        }

        public void AttemptProcessAdd(int processId)
        {
            int idx = -1;
            int tempIdx = 0;
            bool alreadyWhitelisted = whitelistSet.Contains(processId);
            if (alreadyWhitelisted) return;

            foreach(ProcessInfo info in blockedProcessList)
            {
                if (info.PID == processId)
                {
                    idx = tempIdx;
                    break;
                }

                tempIdx++;
            }

            if (idx >= 0)
            {
                whitelistIds.Add(blockedProcessList[idx]);
                whitelistSet.Add(processId);
                blockedProcessList.RemoveAt(idx);
                ExportIdToRegistry(processId);
            }
            else
            {
                try
                {
                    using (Process tmpProc = Process.GetProcessById(processId))
                    {
                        whitelistIds.Add(new ProcessInfo(tmpProc));
                    }

                    whitelistSet.Add(processId);
                    ExportIdToRegistry(processId);
                }
                catch (ArgumentException) { }
            }
        }

        private void ExportIdToRegistry(int pid)
        {
            Util.ExportIdToRegistry(pid);
        }

        private void DeleteIdFromRegistry(int pid)
        {
            Util.DeleteIdFromRegistry(pid);
        }

        private void PurgeWhitelistEntries()
        {
            Util.PurgeWhitelistEntries();
        }
    }

    class ProcessInfo
    {
        private string name;
        public string Name { get => name; }

        private int pid;
        public int PID { get => pid; }

        public ProcessInfo(Process process)
        {
            name = process.ProcessName;
            pid = process.Id;
        }
    }
}
