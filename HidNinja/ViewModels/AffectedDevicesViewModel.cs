﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace HidNinja.ViewModels
{
    class AffectedDevicesViewModel
    {
        private ObservableCollection<string> affectedHwidsCol;
        public ObservableCollection<string> AffectedHwidsCol { get => affectedHwidsCol; }

        private int selectedIndex = -1;
        public int SelectedIndex
        {
            get => selectedIndex;
            set
            {
                if (selectedIndex == value) return;
                selectedIndex = value;
                SelectedIndexChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler SelectedIndexChanged;
        public bool ActionPanelEnabled { get => selectedIndex >= 0; }
        public event EventHandler ActionPanelEnabledChanged;

        public AffectedDevicesViewModel()
        {
            affectedHwidsCol = new ObservableCollection<string>();

            //string hids = "";
            using (RegistryKey key =
                Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters"))
            {
                if (key != null)
                {
                    string[] affectDevs = (string[])key.GetValue("AffectedDevices") ?? new string[0] { };
                    foreach (string device in affectDevs)
                    {
                        //hids += device + ";";
                        affectedHwidsCol.Add(device);
                    }
                }
            }

            AffectedHwidsCol.CollectionChanged += AffectedHwidsCol_CollectionChanged;
            SelectedIndexChanged += AffectedDevicesViewModel_SelectedIndexChanged;
        }

        private void AffectedDevicesViewModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ActionPanelEnabledChanged.Invoke(this, EventArgs.Empty);
        }

        private void AffectedHwidsCol_CollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            ExportAffectedDevices();
        }

        public void ExportAffectedDevices()
        {
            using (RegistryKey key =
                Registry.LocalMachine.CreateSubKey(@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters"))
            {
                key.SetValue("AffectedDevices",
                    affectedHwidsCol.ToArray(), RegistryValueKind.MultiString);
            }
        }

        public void CheckAddHWID(string hwid)
        {
            if (!string.IsNullOrWhiteSpace(hwid) && !affectedHwidsCol.Contains(hwid))
            {
                string checkHwid = $"#{hwid}";
                if (affectedHwidsCol.Contains(checkHwid))
                {
                    int tempIdx = affectedHwidsCol.IndexOf(checkHwid);
                    string temp = affectedHwidsCol[tempIdx];
                    affectedHwidsCol[tempIdx] = temp.Remove(0, 1);
                }
                else
                {
                    affectedHwidsCol.Add(hwid);
                }
            }
        }

        public void RemoveHWIDAt(int idx)
        {
            if (idx < affectedHwidsCol.Count)
            {
                affectedHwidsCol.RemoveAt(idx);
            }
        }

        public bool CommentHWIDAt(int idx)
        {
            bool result = false;
            if (idx < affectedHwidsCol.Count)
            {
                string tmp = affectedHwidsCol[idx];
                if (!tmp.StartsWith("#"))
                {
                    tmp = $"#{tmp}";
                    affectedHwidsCol[idx] = tmp;
                    result = true;
                }
            }

            return result;
        }

        public bool UnCommentHWIDAt(int idx)
        {
            bool result = false;
            if (idx < affectedHwidsCol.Count)
            {
                string tmp = affectedHwidsCol[idx];
                if (tmp.StartsWith("#"))
                {
                    tmp = tmp.Remove(0, 1);
                    affectedHwidsCol[idx] = tmp;
                    result = true;
                }
            }

            return result;
        }
    }
}
