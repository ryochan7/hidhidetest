﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HidNinja.ViewModels
{
    class ManualEditViewModel
    {
        private bool fullEdit;
        public bool FullEdit { get => fullEdit; }

        private string hardwareIdsString;
        public string HardwareIdsString {
            get => hardwareIdsString;
            set
            {
                if (hardwareIdsString == value) return;
                hardwareIdsString = value;
                HardwareIdsStringChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler HardwareIdsStringChanged;

        public ManualEditViewModel(string[] hardwareIds, bool fullEdit)
        {
            hardwareIdsString = string.Join("\r\n", hardwareIds);
            this.fullEdit = fullEdit;
        }
    }
}
