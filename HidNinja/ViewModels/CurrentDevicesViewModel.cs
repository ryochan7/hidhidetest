﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HidLibrary;
using Microsoft.Win32;

namespace HidNinja.ViewModels
{
    class CurrentDevicesViewModel
    {
        private List<DeviceEntry> deviceEntries = new List<DeviceEntry>();
        public List<DeviceEntry> DeviceEntries { get => deviceEntries; }

        private int selectedIndex = -1;
        public int SelectedIndex {
            get => selectedIndex;
            set
            {
                selectedIndex = value;
                SelectedIndexChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler SelectedIndexChanged;

        private DeviceEntry currentDevice;
        public DeviceEntry CurrentDevice { get => currentDevice; }
        public event EventHandler CurrentDeviceChanged;

        public bool CanAdd { get => selectedIndex >= 0; }
        public event EventHandler CanAddChanged;

        public CurrentDevicesViewModel()
        {
            foreach (HidDeviceWrapper wrapper in
                DeviceEnumeration.EnumerateDevices().OrderBy(p => p.UsedName))
            {
                deviceEntries.Add(new DeviceEntry(wrapper));
                //Console.WriteLine(wrapper);
            }

            SelectedIndexChanged += CurrentDevicesViewModel_SelectedIndexChanged;
        }

        private void CurrentDevicesViewModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectedIndex != -1)
            {
                currentDevice = deviceEntries[selectedIndex];
            }
            else
            {
                currentDevice = null;
            }

            CurrentDeviceChanged?.Invoke(this, EventArgs.Empty);
            CanAddChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    class DeviceEntry
    {
        private HidDeviceWrapper dev;

        private string deviceName;
        public string DeviceName => dev.UsedName;
        public List<string> HardwareIds => dev.HardwareIds;

        private int selectedIndex;
        public int SelectedIndex {
            get => selectedIndex;
            set
            {
                if (selectedIndex == value) return;
                selectedIndex = value;
                SelectedIndexChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler SelectedIndexChanged;

        private bool affected;
        public bool IsAffected {
            get => affected;
            private set
            {
                if (affected == value) return;
                affected = value;
                IsAffectedChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler IsAffectedChanged;

        public string VendorId { get => dev.HidDev.Attributes.VendorHexId; }
        public string ProductId { get => dev.HidDev.Attributes.ProductHexId; }
        public string IsAffectedStr
        {
            get
            {
                return affected ? "Yes" : "No";
            }
        }
        public event EventHandler IsAffectedStrChanged;

        public DeviceEntry(HidDeviceWrapper current)
        {
            dev = current;
            deviceName = current.UsedName;

            IsAffectedChanged += DeviceEntry_IsAffectedChanged;
        }

        private void DeviceEntry_IsAffectedChanged(object sender, EventArgs e)
        {
            IsAffectedStrChanged?.Invoke(this, EventArgs.Empty);
        }

        public void RefreshAffectedState(ObservableCollection<string> affectedIds)
        {
            bool tmp = false;
            foreach (string id in dev.HardwareIds)
            {
                if (affectedIds.Contains(id))
                {
                    tmp = true;
                    break;
                }
            }

            IsAffected = tmp;
        }
    }
}
