﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HidNinja.ViewModels;

namespace HidNinja
{
    /// <summary>
    /// Interaction logic for ManualEditWindow.xaml
    /// </summary>
    public partial class ManualEditWindow : Window
    {
        private bool fullEdit;
        private ManualEditViewModel mwd;

        public delegate void SavedHandler(ManualEditWindow sender, string outputHWIDs);
        public event SavedHandler Saved;

        public ManualEditWindow(string[] hardwareIds, bool bulk=false)
        {
            InitializeComponent();

            fullEdit = bulk;
            if (!fullEdit) hardwareIdsTxtBox.AcceptsReturn = false;

            mwd = new ManualEditViewModel(hardwareIds, bulk);
            hardwareIdsTxtBox.DataContext = mwd;
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            hardwareIdsTxtBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            Saved?.Invoke(this, mwd.HardwareIdsString);
            Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
