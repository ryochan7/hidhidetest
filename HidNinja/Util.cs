﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace HidNinja
{
    static class Util
    {
        private static int mainProcessId = Process.GetCurrentProcess().Id;
        public static int MainProcessId { get => mainProcessId; }

        public static void PurgeWhitelistEntries(bool checkRunning=false)
        {
            using (RegistryKey key =
                Registry.LocalMachine.OpenSubKey($@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters\Whitelist\", false))
            {
                foreach (string tmp in key.GetSubKeyNames())
                {
                    if (int.TryParse(tmp, out int pid) && pid != mainProcessId)
                    {
                        if (checkRunning)
                        {
                            try
                            {
                                using (Process tempProc = Process.GetProcessById(pid))
                                {
                                }
                            }
                            catch (ArgumentException)
                            {
                                // No process with PID running. Delete entry
                                DeleteIdFromRegistry(pid);
                            }
                        }
                        else
                        {
                            DeleteIdFromRegistry(pid);
                        }
                    }
                }
            }
        }

        public static void DeleteIdFromRegistry(int pid)
        {
            Registry.LocalMachine.DeleteSubKey($@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters\Whitelist\{pid}", false);
        }

        public static void ExportIdToRegistry(int pid)
        {
            using (RegistryKey key =
                Registry.LocalMachine.CreateSubKey($@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters\Whitelist\{pid}",
                RegistryKeyPermissionCheck.Default, RegistryOptions.Volatile))
            {
            }
        }

        public static void WriteWhitelistRoot()
        {
            using (RegistryKey key =
                Registry.LocalMachine.CreateSubKey($@"SYSTEM\CurrentControlSet\services\HidGuardian\Parameters\Whitelist"))
            {
            }
        }
    }
}
