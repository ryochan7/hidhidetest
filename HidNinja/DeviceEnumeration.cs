﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using HidLibrary;
using Microsoft.Win32;

namespace HidNinja
{
    [System.Security.SuppressUnmanagedCodeSecurity]
    static class DeviceEnumeration
    {
        public static IEnumerable<HidDeviceWrapper> EnumerateDevices()
        {
            IEnumerable<HidDevices.DeviceInfo> devices = HidDevices.EnumerateDevices();
            List<HidDeviceWrapper> resultList = new List<HidDeviceWrapper>();
            foreach(HidDevices.DeviceInfo dev in devices)
            {
                HidDevice tmp = new HidDevice(dev.Path, dev.Description);
                HidDeviceWrapper wrapper = new HidDeviceWrapper(tmp);
                string devInst = DevicePathToInstanceId(tmp.DevicePath);
                wrapper.PopulateFromDev(devInst);

                resultList.Add(wrapper);
            }

            return resultList;
        }

        private static string DevicePathToInstanceId(string devicePath)
        {
            string deviceInstanceId = devicePath;
            deviceInstanceId = deviceInstanceId.Remove(0, deviceInstanceId.LastIndexOf("?\\") + 2);
            deviceInstanceId = deviceInstanceId.Remove(deviceInstanceId.LastIndexOf('{'));
            deviceInstanceId = deviceInstanceId.Replace('#', '\\');
            if (deviceInstanceId.EndsWith("\\"))
            {
                deviceInstanceId = deviceInstanceId.Remove(deviceInstanceId.Length - 1);
            }

            return deviceInstanceId;
        }
    }

    public class HidDeviceWrapper
    {
        private HidDevice hidDev;
        public HidDevice HidDev { get => hidDev; }

        private string usedName;
        public string UsedName { get => usedName; }
        
        private List<string> hardwareIds = new List<string>();
        public List<string> HardwareIds { get => hardwareIds; }

        public HidDeviceWrapper(HidDevice hidDevice)
        {
            hidDev = hidDevice;
        }

        public void PopulateFromDev(string deviceInstanceId)
        {
            bool success;
            Guid hidGuid = HidDevices.HidClassGuid;
            IntPtr deviceInfoSet = NativeMethods.SetupDiGetClassDevs(ref hidGuid, deviceInstanceId, 0, NativeMethods.DIGCF_PRESENT | NativeMethods.DIGCF_DEVICEINTERFACE);

            if (deviceInfoSet.ToInt64() != NativeMethods.INVALID_HANDLE_VALUE)
            {
                NativeMethods.SP_DEVINFO_DATA deviceInfoData = new NativeMethods.SP_DEVINFO_DATA();
                deviceInfoData.cbSize = Marshal.SizeOf(deviceInfoData);
                success = NativeMethods.SetupDiEnumDeviceInfo(deviceInfoSet, 0, ref deviceInfoData);

                var requiredSize = 0;
                ulong propertyType = 0;

                NativeMethods.SetupDiGetDeviceProperty(deviceInfoSet, ref deviceInfoData,
                                                        ref NativeMethods.DEVPKEY_Device_HardwareIds, ref propertyType,
                                                        null, 0,
                                                        ref requiredSize, 0);

                if (requiredSize > 0)
                {
                    var descriptionBuffer = new byte[requiredSize];
                    NativeMethods.SetupDiGetDeviceProperty(deviceInfoSet, ref deviceInfoData,
                                                            ref NativeMethods.DEVPKEY_Device_HardwareIds, ref propertyType,
                                                            descriptionBuffer, descriptionBuffer.Length,
                                                            ref requiredSize, 0);

                    string tmpitnow = Encoding.Unicode.GetString(descriptionBuffer);
                    string tempStrip = tmpitnow.TrimEnd('\0');
                    string[] tmparray = tempStrip.Split((char)0);
                    hardwareIds.AddRange(tmparray);
                }

                usedName = hidDev.Description;
                switch (hidDev.Capabilities.Usage)
                {
                    case 0x04:
                    case 0x05:
                        usedName = GetOEMName() ?? hidDev.Description;
                        break;
                    default:
                        break;
                }

                byte[] tmpBuff = Encoding.Unicode.GetBytes(usedName);
                usedName = Encoding.Unicode.GetString(tmpBuff);
                NativeMethods.SetupDiDestroyDeviceInfoList(deviceInfoSet);
            }
        }

        private string GetOEMName()
        {
            string oemname = null;
            string hid = $"VID_{hidDev.Attributes.VendorId:X4}&PID_{hidDev.Attributes.ProductId:X4}";
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\MediaProperties\PrivateProperties\Joystick\OEM\" + hid))
            {
                if (key != null)
                    oemname = key.GetValue("OEMName")?.ToString();
            }
            return oemname;
        }
    }
}
